# Newlife.CoreLibrary
# 概述

CoreLibrary 类库提供了.NET开发的常用功能集以及一些相对于.NET BCL中进行了功能强化的实现。采用C#语言开发，支持跨平台可运行在 Windows、Windows Phone 8.x 以及 Linux(Mono) 等平台中。